/*:
Welcome to the mBot Playground!

Follow these steps to see what the mBot can do:
 
1. Click the "Run My Code" Button;
2. Hold your iPad near the mBot, you will see the text on
the right side change to "Connected";
3. touch the "Start" button, and enjoy.
 
This line of code will be run on the mBot:
*/
//#-hidden-code
import PlaygroundSupport
import Foundation

let viewController = BasicControlViewController()


viewController.listenStartButtonClicked = { sender in
    let mBot: BasicCommands = BasicCommands(robot: viewController.mBot)
    
//#-end-hidden-code
//#-editable-code
    mBot.helloWorld()
//#-end-editable-code
//#-hidden-code
}

PlaygroundPage.current.liveView = viewController

//#-end-hidden-code
