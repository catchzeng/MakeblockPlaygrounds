/*:
 You can move the mBot around with these commands: 
 * `forward()`: the mBot will move forward;
 * `back()`: the mBot will move backward;
 * `left()`: the mBot will turn left;
 * `right()`: the mBot will turn right.
 
 Try them by yourself!
 
*/
//#-hidden-code
import PlaygroundSupport
import Foundation

let viewController = BasicControlViewController()


viewController.listenStartButtonClicked = { sender in
    let mBot: BasicCommands = BasicCommands(robot: viewController.mBot)
    
    func forward() {
        mBot.forward()
    }
    
    func back() {
        mBot.back()
    }
    
    func left() {
        mBot.back()
    }
    
    func right() {
        mBot.back()
    }
    


//#-code-completion(everything, hide)
//#-code-completion(identifier, show, forward(), back(), left(), right())
    //#-end-hidden-code
//#-editable-code Tap to write your code
//#-end-editable-code
//#-hidden-code
}

PlaygroundPage.current.liveView = viewController

//#-end-hidden-code
