//
//  ViewController.swift
//  MakeblockPlaygrounds
//
//  Created by CatchZeng on 2016/12/8.
//  Copyright © 2016年 makeblock. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private let connection = BluetoothConnection()
    private var mBot:MBot?
    private var controlCtrl:ContorlViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mBot = MBot(connection:connection)
        mBot?.nearest()
        
        connection.onStateChanged = { state in
            if state == BluetoothConnectionState.idle{
                self.controlCtrl?.setHintInfo(content:Message.nearmBot.rawValue)
            }
            else if state == BluetoothConnectionState.connected{
                self.controlCtrl?.setHintInfo(content:Message.connected.rawValue)
                self.mBot?.getUltrasonicSensorValue(callback: { (value) in
                    print("超声波的值为:\(value)")
                })
            }
            else if state == BluetoothConnectionState.connecting{
                self.controlCtrl?.setHintInfo(content:Message.connecting.rawValue)
            }
        }
    }
    
    @IBAction func actionmBot(_ sender: Any) {
//        controlCtrl = ContorlViewController()
//        self.controlCtrl?.defaultHintInfo = Message.nearmBot.rawValue
//        self.navigationController?.pushViewController(controlCtrl!, animated: true);
//        
//        controlCtrl?.joystickMoved = {angle,magnitude,targetPoint,centerPoint,radius in
//            self.mBot?.joystickMoved(angle: angle, magnitude: magnitude, targetPoint: targetPoint, centerPoint: centerPoint, radius: radius)
//        }
//        
//        controlCtrl?.colorSelected = {color in
//            self.mBot?.setRGBLED(position: .all, color: color)
//            self.mBot?.setRGBLED(position: .all, red: 0, green: 0, blue: 0)
//        }
//        let vc =  MusicViewController()
//        self.navigationController?.pushViewController(vc, animated: true);
//        vc.listenMusicKeyClicked = { type in
//            if type == .Do{
//                self.mBot?.setBuzzer(pitch: MBot.MusicNotePitch.C5, duration: MBot.MusicNoteDuration.quarter)
//            }else if type == .Re{
//                self.mBot?.setBuzzer(pitch: MBot.MusicNotePitch.D5, duration: MBot.MusicNoteDuration.quarter)
//            }else if type == .Mi{
//                self.mBot?.setBuzzer(pitch: MBot.MusicNotePitch.E5, duration: MBot.MusicNoteDuration.quarter)
//            }else if type == .Fa{
//                self.mBot?.setBuzzer(pitch: MBot.MusicNotePitch.F5, duration: MBot.MusicNoteDuration.quarter)
//            }else if type == .Sol{
//                self.mBot?.setBuzzer(pitch: MBot.MusicNotePitch.G5, duration: MBot.MusicNoteDuration.quarter)
//            }else if type == .La{
//                self.mBot?.setBuzzer(pitch: MBot.MusicNotePitch.A5, duration: MBot.MusicNoteDuration.quarter)
//            }else if type == .Si{
//                self.mBot?.setBuzzer(pitch: MBot.MusicNotePitch.B5, duration: MBot.MusicNoteDuration.quarter)
//            }
//        }
        
        let vc =  LEDControlViewController()
        self.navigationController?.pushViewController(vc, animated: true);
    }
}
