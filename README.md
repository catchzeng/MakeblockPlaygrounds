#MakeblockPlaygrounds
----
### Makeblock.playgroundbook
此文件为可在Playgrounds App上跑的项目文件,可通过Airdrop下发该文件到iPad上，即可打开项目

### MakeblockPlaygrounds
本项目是开发和调试Makeblock.playgroundbook的工程。开发完毕后，将Makeblock.playgroundbook所需的文件拷贝过去就可在iPad上执行

### Reference
此文件夹放置了Sphero和Files的参考项目，便于入手

### 其他资源

[王宇的Makeblock swift版framework](https://github.com/Makeblock-official/Makeblock-Swift)

[苹果官方swift playgrounds 文档](https://developer.apple.com/library/prerelease/content/documentation/Xcode/Conceptual/swift_playgrounds_doc_format/BookManifest.html#//apple_ref/doc/uid/TP40017343-CH63-SW8 
)